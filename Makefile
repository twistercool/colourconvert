VERSION = 0.1
INCS = -Iinclude
LIBS = -lm
CXXFLAGS = -std=c++20 ${INCS} -march=native -O2 -fno-exceptions -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable
DEBUG_CXXFLAGS = -std=c++20 ${INCS} -fno-exceptions -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -DDEBUG -Og -g3 -ggdb
LDFLAGS = ${LIBS} -flto
DEBUG_LDFLAGS = ${LIBS}
TEST_LDFLAGS = ${DEBUG_LDFLAGS} -lcriterion
CXX = clang++
STRIP = strip

SRC = $(wildcard src/*.cpp)
release_OBJ = $(patsubst src/%.cpp, target/release/%.o, ${SRC})
debug_OBJ = $(patsubst src/%.cpp, target/debug/%.o, ${SRC})
SRC_TEST = $(wildcard tests/*.cpp)
OBJ_TEST = ${SRC_TEST:.cpp=.o}

target/release/%.o: src/%.cpp
	@mkdir -p target/release
	${CXX} ${CXXFLAGS} -c $< -o $@

target/release/colourconvert: ${release_OBJ}
	${CXX} $^ ${CXXFLAGS} ${LDFLAGS} -o $@
	${STRIP} $@

target/debug/%.o: src/%.cpp
	@mkdir -p target/debug
	${CXX} ${DEBUG_CXXFLAGS} -c $< -o $@

target/debug/colourconvert: ${debug_OBJ}
	${CXX} $^ ${DEBUG_CXXFLAGS} ${DEBUG_LDFLAGS} -o $@

tests/test: $(filter-out %/main.o, ${debug_OBJ}) ${SRC_TEST}
	${CXX} $^ ${DEBUG_CXXFLAGS} ${TEST_LDFLAGS} -o tests/test
	@echo -e '--RUNNING TESTS--'
	@./tests/test -j0 2>&1 | sed 's/^\[----\] .*:.*://g' | sed 's/^boxfort-worker: //g' || true
	@rm tests/test

clean:
	@echo -e '--CLEANING--'
	rm -f target/release/* target/debug/* ${OBJ_TEST}

all: target/release/colourconvert

release: target/release/colourconvert

debug: target/debug/colourconvert

test: tests/test

.PHONY: all release debug test clean
