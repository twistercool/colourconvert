#include "colorconvert.hpp"
#include <criterion/criterion.h>

Test(color_helpers, hex_to_int) {
  cr_assert_eq(hex_to_int("00"), 0);
  cr_assert_eq(hex_to_int("bf"), 191);
  cr_assert_eq(hex_to_int("ff"), 255);
}

Test(color_helpers, clamb) {
  cr_assert_eq(clamp(5, 2, 10), 5);
  cr_assert_eq(clamp(2, 5, 10), 5);
  cr_assert_eq(clamp(10, 2, 5), 5);
}

Test(color_helpers, hue_to_rgb_comp) {
  cr_assert_float_eq(hue_to_rgb_comp(0.2, 0.8, 0.1), 0.56, 1e-6);
  cr_assert_float_eq(hue_to_rgb_comp(0.2, 0.8, 0.3), 0.8, 1e-6);
  cr_assert_float_eq(hue_to_rgb_comp(0.2, 0.8, 0.5), 0.8, 1e-6);
  cr_assert_float_eq(hue_to_rgb_comp(0.2, 0.8, 0.5), 0.8, 1e-6);
}

Test(color_convert, hex_to_rgb) {
  char rgb[MAX_STR_LEN];
  hex_to_rgb("#000000", rgb);
  cr_assert_str_eq(rgb, "0,0,0");
  hex_to_rgb("#ffffff", rgb);
  cr_assert_str_eq(rgb, "255,255,255");
  hex_to_rgb("#00bfff", rgb);
  cr_assert_str_eq(rgb, "0,191,255");
  hex_to_rgb("#3c140a", rgb);
  cr_assert_str_eq(rgb, "60,20,10");
  hex_to_rgb("#3cb43c", rgb);
  cr_assert_str_eq(rgb, "60,180,60");
}

Test(color_convert, hsl_to_rgb) {
  char rgb[MAX_STR_LEN];
  hsl_to_rgb("0,0,0", rgb);
  cr_assert_str_eq(rgb, "0,0,0");
  hsl_to_rgb("0,0,100", rgb);
  cr_assert_str_eq(rgb, "255,255,255");
  hsl_to_rgb("195,100,50", rgb);
  cr_assert_str_eq(rgb, "0,191,255");
  hsl_to_rgb("12,70,14", rgb);
  cr_assert_str_eq(rgb, "60,20,10");
  hsl_to_rgb("120,50.1,47.25", rgb);
  cr_assert_str_eq(rgb, "60,180,60");
}

Test(color_convert, percent_to_rgb) {
  char rgb[MAX_STR_LEN];
  percent_to_rgb("0.0,0.0,0.0", rgb);
  cr_assert_str_eq(rgb, "0,0,0");
  percent_to_rgb("100.0,100.0,100.0", rgb);
  cr_assert_str_eq(rgb, "255,255,255");
  percent_to_rgb("0.0,74.91,100.0", rgb);
  cr_assert_str_eq(rgb, "0,191,255");
  percent_to_rgb("23.53,7.85,3.95", rgb);
  cr_assert_str_eq(rgb, "60,20,10");
  percent_to_rgb("23.53,70.59,23.53", rgb);
  cr_assert_str_eq(rgb, "60,180,60");
}

Test(color_convert, ratio_to_rgb) {
  char rgb[MAX_STR_LEN];
  ratio_to_rgb("0.0,0.0,0.0", rgb);
  cr_assert_str_eq(rgb, "0,0,0");
  ratio_to_rgb("1.0,1.0,1.0", rgb);
  cr_assert_str_eq(rgb, "255,255,255");
  ratio_to_rgb("0.0,0.7491,1.0", rgb);
  cr_assert_str_eq(rgb, "0,191,255");
  ratio_to_rgb("0.2353,0.0785,0.0395", rgb);
  cr_assert_str_eq(rgb, "60,20,10");
  ratio_to_rgb("0.2353,0.7059,0.2353", rgb);
  cr_assert_str_eq(rgb, "60,180,60");
}

Test(color_convert, rgb_to_hex) {
  char hex[MAX_STR_LEN];
  rgb_to_hex("0,0,0", hex);
  cr_assert_str_eq(hex, "#000000");
  rgb_to_hex("255,255,255", hex);
  cr_assert_str_eq(hex, "#ffffff");
  rgb_to_hex("0,191,255", hex);
  cr_assert_str_eq(hex, "#00bfff");
  rgb_to_hex("60,20,10", hex);
  cr_assert_str_eq(hex, "#3c140a");
  rgb_to_hex("60,180,60", hex);
  cr_assert_str_eq(hex, "#3cb43c");
}

Test(color_convert, rgb_to_hsl) {
  char hsl[MAX_STR_LEN];
  rgb_to_hsl("0,0,0", hsl);
  cr_assert_str_eq(hsl, "0,0,0");
  rgb_to_hsl("255,255,255", hsl);
  cr_assert_str_eq(hsl, "0,0,100");
  rgb_to_hsl("0,191,255", hsl);
  cr_assert_str_eq(hsl, "195,100,50");
  rgb_to_hsl("60,20,10", hsl);
  cr_assert_str_eq(hsl, "12,71,13");
  rgb_to_hsl("60,180,60", hsl);
  cr_assert_str_eq(hsl, "120,50,47");
}

Test(color_convert, rgb_to_percent) {
  char percent[MAX_STR_LEN];
  rgb_to_percent("0,0,0", percent);
  cr_assert_str_eq(percent, "0,0,0");
  rgb_to_percent("255,255,255", percent);
  cr_assert_str_eq(percent, "100,100,100");
  rgb_to_percent("0,191,255", percent);
  cr_assert_str_eq(percent, "0,74,100");
  rgb_to_percent("60,20,10", percent);
  cr_assert_str_eq(percent, "23,7,3");
  rgb_to_percent("60,180,60", percent);
  cr_assert_str_eq(percent, "23,70,23");
}

Test(color_convert, rgb_to_ratio) {
  char ratio[MAX_STR_LEN];
  rgb_to_ratio("0,0,0", ratio);
  cr_assert_str_eq(ratio, "0.00,0.00,0.00");
  rgb_to_ratio("255,255,255", ratio);
  cr_assert_str_eq(ratio, "1.00,1.00,1.00");
  rgb_to_ratio("0,191,255", ratio);
  cr_assert_str_eq(ratio, "0.00,0.75,1.00");
  rgb_to_ratio("60,20,10", ratio);
  cr_assert_str_eq(ratio, "0.24,0.08,0.04");
  rgb_to_ratio("60,180,60", ratio);
  cr_assert_str_eq(ratio, "0.24,0.71,0.24");
}
