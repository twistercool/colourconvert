#include "colourconvert.hpp"

/**
 * Convert a hue component to its corresponding RGB component
 *
 * # Parameters
 * - p: Intermediate RGB value
 * - q: Intermediate RGB value
 * - t: Hue value adjusted for RGB conversion
 *
 * # Return
 * Calculated RGB component for the hue
 */
constexpr double
hue_to_rgb_comp(double p, double q, double t) {
  if (t < 0.0) t += 1.0;
  if (t > 1.0) t -= 1.0;
  if (t < 1.0 / 6.0) return p + (q - p) * 6.0 * t;
  if (t < 1.0 / 2.0) return q;
  if (t < 2.0 / 3.0) return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
  return p;
}

/**
 * Remove the spaces from a given string
 *
 * # Parameters
 * - value: Input string
 * - buffer: Output buffer to store the trimmed string
 *
 * # Return
 * 0 on sucess, 1 on failure
 */
int
trim(const char *value, char *buffer) {
  if (value == NULL || buffer == NULL) { return 1; }

  size_t input_len = strlen(value);

  if (input_len >= MAX_STR_LEN - 1) { return 1; }

  size_t n = 0;
  for (size_t i = 0; i < input_len && i < MAX_STR_LEN - 1; i++) {
    if (value[i] != ' ') {
      buffer[n] = value[i];
      n++;
    }
  }
  buffer[n] = '\0';

  return 0;

// perso moi j'ecrirais ca comme ca (traverse le string juste une fois):
   if (value == NULL || buffer == NULL) { return 1; }

   size_t k = 0;
   while (*value && k < MAX_STR_LEN - 1) {
      if (*value != ' ') {
         *buffer++ = *value++;
         k++;
      } else {
         value++;
      }
   }
   if (k == MAX_STR_LEN) {
      if (MAX_STR_LEN > 0)
         buffer[0] = '\0';
      return 1;
   } else {
      *buffer = '\0';
      return 0;
   }
}

struct colour
parse_rgb(const char* in) {
   struct colour ret{};

   if (sscanf(in, "%hhu , %hhu , %hhu", &ret.r, &ret.g, &ret.b) != 3) {
      ret.err = parse_error::ParseRgb;
      return ret;
   }

   return ret;
}

struct colour
parse_hex(const char *in) {
   struct colour ret{};

   if (in[0] == '#') ++in;

   size_t len = strlen(in);

   if (len != 6 || len >= MAX_STR_LEN) {
      ret.err = parse_error::ParseHex;
      return ret;
   }

   if (sscanf(in, "%02hhx %02hhx %02hhx", &ret.r, &ret.g, &ret.b) != 3) {
      ret.err = parse_error::ParseHex;
      return ret;
   }

   return ret;
}

struct colour
parse_hsl(const char *value) {
   struct colour ret;
   double h, s, l;
   if (sscanf(value, "%lf,%lf,%lf", &h, &s, &l) != 3) {
      ret.err = parse_error::ParseHsl;
      return ret;
   }

   h /= 360.0;
   s /= 100.0;
   l /= 100.0;
  
   if (h < 0.0 || h > 1.0 || s < 0.0 || s > 1.0 || l < 0.0 || l > 1.0) {
      ret.err = parse_error::ParseHsl;
      return ret;
   }

   double r, g, b;
   if (s == 0) {
      r = g = b = l;
   } else {
      double q = (l < 0.5) ? l * (1.0 + s) : l + s - l * s;
      double p = 2.0 * l - q;
      r = hue_to_rgb_comp(p, q, h + 1.0 / 3.0);
      g = hue_to_rgb_comp(p, q, h);
      b = hue_to_rgb_comp(p, q, h - 1.0 / 3.0);
   }

   r *= 255.0;
   g *= 255.0;
   b *= 255.0;

   ret.r = static_cast<uint8_t>(r);
   ret.g = static_cast<uint8_t>(g);
   ret.b = static_cast<uint8_t>(b);
   ret.err = parse_error::None;

   return ret;
}

struct colour
parse_percent(const char *in) {
   struct colour ret;

   double pr, pg, pb;
   if (sscanf(in, "%lf , %lf , %lf", &pr, &pg, &pb) != 3) {
      ret.err = parse_error::ParsePercent;
      return ret;
   }

   if (pr < 0.0 || pr > 100.0 || pg < 0.0 || pg > 100.0 || pb < 0.0 || pb > 100.0) {
      ret.err = parse_error::ParsePercent;
      return ret;
   }

   double r = 255.0 * pr / 100.0;
   double g = 255.0 * pg / 100.0;
   double b = 255.0 * pb / 100.0;

   ret.r = static_cast<uint8_t>(r);
   ret.g = static_cast<uint8_t>(g);
   ret.b = static_cast<uint8_t>(b);
   ret.err = parse_error::None;

   return ret;
}

struct colour
parse_ratio(const char *in) {
   struct colour ret;
   float rr, rg, rb;
   if (sscanf(in, "%f , %f , %f", &rr, &rg, &rb) != 3) {
      ret.err = parse_error::ParseRatio;
      return ret;
   }

   if (rr < 0.0 || rr > 1.0 || rg < 0.0 || rg > 1.0 || rb < 0.0 || rb > 1.0) {
      ret.err = parse_error::ParseRatio;
      return ret;
   }

   double r = 255.0 * rr;
   double g = 255.0 * rg;
   double b = 255.0 * rb;

   ret.r = static_cast<uint8_t>(r);
   ret.g = static_cast<uint8_t>(g);
   ret.b = static_cast<uint8_t>(b);
   ret.err = parse_error::None;

   return ret;
}

void
print_colour_as_rgb(const struct colour c) {
   std::cout << "rgb: " << +c.r << "," << +c.g << "," << +c.b << " ; ";
}

void
print_colour_as_hex(const struct colour c) {
   std::ios_base::fmtflags f{ std::cout.flags() };
   std::cout << std::hex << std::setw(2) << std::setfill('0') << "hex: #" << +c.r << +c.g << +c.b << " ; ";
   std::cout.flags(f);
}

void
print_colour_as_hsl(const struct colour c) {
   double tr = (double) c.r / 255.0;
   double tg = c.g / 255.0;
   double tb = c.b / 255.0;

   double h = 0, s = 0, l = 0;
   double max = std::max({tr, tg, tb});
   double min = std::min({tr, tg, tb});
   double delta = (max - min);

   if (delta == 0) {
      h = 0;
   } else if (max == tr) {
      h = std::fmod(((tg - tb) / delta), 6);
   } else if (max == tg) {
      h = ((tb - tr) / delta) + 2.0;
   } else if (max == tb) {
      h = ((tr - tg) / delta) + 4.0;
   }

   l = (max + min) / 2.0;

   if (l == 0.0 || l == 1.0) {
      s = 0;
   } else {
      s = delta / (1 - std::fabs(2 * l - 1));
   }

   std::cout << "hsl: " << static_cast<int>(h * 60) << "," << static_cast<int>(s * 100) << "," << static_cast<int>(l * 100) << " ; ";
}

void
print_colour_as_percent(const struct colour c) {
   std::cout << "percent: " << static_cast<int>(100.0 * c.r / 255.0) << "," << static_cast<int>(100.0 * c.g / 255.0) << "," << static_cast<int>(100.0 * c.b / 255.0) << " ; ";
}

void
print_colour_as_ratio(const struct colour c) {
   std::ios_base::fmtflags f{ std::cout.flags() };
   std::cout << std::setprecision(2) << std::fixed << "ratio: " << c.r / 255.0 << "," << c.g / 255.0 << "," << c.b / 255.0 << "\n";
   std::cout.flags(f);
}
