#include "colourconvert.hpp"

void parse_args(int argc, const char *argv[]);
void print_help(void);
void print_colour(struct colour);

/**
 * Take command line argument for various colour formats, process them, and print the colours in various representation
 *
 * # Parameters
 * - argc: Number of command line arguments
 * - argv: Array of command line arguments
 *
 * # Return
 * EXIT_SUCCESS on success, EXIT_FAILURE on failure
 */
int
main(int argc, const char *argv[]) {
  if (argc < 2) {
    std::cerr << "At least one argument is required" << std::endl;
    exit(EXIT_FAILURE);
  }

  parse_args(argc, argv);
}

/**
 * Parse the colour arguments and print the various colour formats
 *
 * # Parameters
 * - argc: Number of command line arguments
 * - argv: Array of command line arguments
 */
void
parse_args(int argc, const char *argv[]) {
   for (int i = 1; i < argc; i++) {
      const char* cur_flag = argv[i];
      enum conversion_type conv;
      if (!strcmp(cur_flag, "--rgb")) { conv = conversion_type::from_rgb; }
      else if (!strcmp(cur_flag, "--hex")) { conv = conversion_type::from_hex; }
      else if (!strcmp(cur_flag, "--hsl")) { conv = conversion_type::from_hsl; }
      else if (!strcmp(cur_flag, "--percent")) { conv = conversion_type::from_percent; }
      else if (!strcmp(cur_flag, "--ratio")) { conv = conversion_type::from_ratio; }
      else if (!strcmp(cur_flag, "--help")) {
         print_help();
         continue;
      } else {
         std::cerr << "error: " << cur_flag << " did not match any arguments\n";
         exit(EXIT_FAILURE);
      }

      const char* cur_arg = argv[++i];
      if (cur_arg == nullptr) {
         std::cerr << cur_flag << " requires a value." << std::endl;
         exit(EXIT_FAILURE);
      }

      struct colour colour{};
      switch (conv) {
         case conversion_type::from_rgb: { colour = parse_rgb(cur_arg); } break;
         case conversion_type::from_hex: { colour = parse_hex(cur_arg); } break;
         case conversion_type::from_hsl: { colour = parse_hsl(cur_arg); } break;
         case conversion_type::from_percent: { colour = parse_percent(cur_arg); } break;
         case conversion_type::from_ratio: { colour = parse_ratio(cur_arg); } break;
      }

      if (colour.err != parse_error::None) {
         std::cerr << "Error with " << cur_flag + 2 << ": " << cur_arg << std::endl;
         exit(EXIT_FAILURE);
      } else {
         print_colour(colour);
      }
   }
}

/*
 * Print help message to the stdout
 */
void
print_help(void) {
   std::cout << "Usage: colourconvert [OPTIONS] [VALUE]\n"
                "Options\n"
                "--rgb      : Specify colour in RGB format\n"
                "--hex      : Specify colour in HEX format\n"
                "--hsl      : Specify colour in HSL format\n"
                "--percent  : Specify colour in percentage format\n"
                "--ratio    : Specify colour in ratio format\n"
                "--help     : Print this help message\n"
                "Example: colourconvert --rgb '255,255,255' --hex '#ffffff' --hsl '0,0,100' --percent '100,100,100' --ratio '1.00,1.00,1.00'"
             << std::endl;
   exit(EXIT_SUCCESS);
}

void
print_colour(const struct colour colour) {
   print_colour_as_rgb(colour);
   print_colour_as_hex(colour);
   print_colour_as_hsl(colour);
   print_colour_as_percent(colour);
   print_colour_as_ratio(colour);
}
