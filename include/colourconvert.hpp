#pragma once

#include <iostream>
#include <iomanip>
#include <algorithm>

#include <string.h>
#include <math.h>
#include <stdint.h>

const uint8_t MAX_STR_LEN = 64;


enum conversion_type: uint8_t {
   from_rgb,
   from_hex,
   from_hsl,
   from_percent,
   from_ratio
};

enum parse_error: uint8_t {
   None = 0,
   ParseRgb,
   ParseHex,
   ParseHsl,
   ParsePercent,
   ParseRatio,
};

struct colour {
   uint8_t r;
   uint8_t g;
   uint8_t b;
   enum parse_error err;
};

struct colour parse_rgb(const char *in);
struct colour parse_hex(const char *in);
struct colour parse_hsl(const char *in);
struct colour parse_percent(const char *in);
struct colour parse_ratio(const char *in);

void print_colour_as_rgb(const struct colour c);
void print_colour_as_hex(const struct colour c);
void print_colour_as_hsl(const struct colour c);
void print_colour_as_percent(const struct colour c);
void print_colour_as_ratio(const struct colour c);

constexpr double hue_to_rgb_comp(double p, double q, double t);
int trim(const char *value, char *buffer);
