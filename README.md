# ColourConvert

ColourConvert is a program to convert and print various color formats: HEX, RGB, HSL, percentage and ratio.

## Usage

```sh
colourconvert [FORMAT_FLAG] [COLOR_VALUE]
```

### Format flags:

- `--hex`: Specify color in HEX format
- `--rgb`: Specify color in RGB format
- `--hsl`: Specify color in HSL format
- `--percent`: Specify color in percentage format
- `--ratio`: Specify color in ratio format

### Example output:

```
$ colourconvert --hex #ffffff --rgb 255,255,255 --hsl 0,0,100 --percent 100,100,100 --ratio 1.0,1.0,1.0
hex: #ffffff ; rgb: 255,255,255 ; hsl: 0,0,100 ; percent: 100,100,100 ; ratio: 1.00,1.00,1.00
hex: #ffffff ; rgb: 255,255,255 ; hsl: 0,0,100 ; percent: 100,100,100 ; ratio: 1.00,1.00,1.00
hex: #ffffff ; rgb: 255,255,255 ; hsl: 0,0,100 ; percent: 100,100,100 ; ratio: 1.00,1.00,1.00
hex: #ffffff ; rgb: 255,255,255 ; hsl: 0,0,100 ; percent: 100,100,100 ; ratio: 1.00,1.00,1.00
hex: #ffffff ; rgb: 255,255,255 ; hsl: 0,0,100 ; percent: 100,100,100 ; ratio: 1.00,1.00,1.00
```

## Installation

```sh
git clone https://git.brassart.com/colourconvert
cd colourconvert
make release
sudo mv target/release/colourconvert /usr/bin/colourconvert
```

## Test

To test this program, do :

```sh
make test
```
   
## Shoutout

Thanks to [Paul Chambaz](https://github.com/paulchambaz) for writing the original code, I simply refactored it here.

## License

This project is licensed under the GPLv3 license.
For more information, read the LICENSE file.
